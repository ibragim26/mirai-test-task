<?php

declare(strict_types=1);

namespace Ibragimov\MiraiTestTask\Tests\Repository;

use Ibragimov\MiraiTestTask\Entity\City;
use Ibragimov\MiraiTestTask\Repository\CityRepository;

/**
 * Class MockCityRepository
 * @author Shapi Ibragimov <ibragimych26@gmail.com>
 */
final class MockCityRepository extends CityRepository
{
    public function getCityById(string $cityId): City
    {
        return self::getTestCity();
    }

    public static function getTestCity(): City
    {
        $city = new City();
        $city->setName('Test');
        $city->setCountryIso3('RUS');
        $city->setLatitude(0.0);
        $city->setLatitude(0.0);

        return $city;
    }
}