<?php

declare(strict_types=1);

namespace Ibragimov\MiraiTestTask\Tests\Repository;

use Doctrine\ORM\EntityManager;
use Ibragimov\MiraiTestTask\Entity\GmtOffset;
use Ibragimov\MiraiTestTask\Repository\GmtOffsetRepository;

/**
 * Class MockOffsetRepository
 * @author Shapi Ibragimov <ibragimych26@gmail.com>
 */
final class MockOffsetRepository extends GmtOffsetRepository
{
    public const HOUR = 3600;

    private int $offset;
    private int $dstStart;
    private int $dstEnd;

    public function __construct(EntityManager $em, int $offset = null, \DateTime $dstStart = null, \DateTime $dstEnd = null)
    {
        $this->dstStart = $dstStart === null ? 0 : $dstStart->getTimestamp();
        $this->dstEnd = $dstEnd === null ? 0 : $dstEnd->getTimestamp();
        $this->offset = $offset ?? self::HOUR;

        parent::__construct($em);
    }

    public function findOffsetForTimestamp(string $cityId, int $timestamp): ?GmtOffset
    {
        $city = MockCityRepository::getTestCity();

        $offset = new GmtOffset($city);
        $offset->setDstStart($this->dstStart);
        $offset->setDstEnd($this->dstEnd);
        $offset->setGmtOffset($this->offset);
        return $offset;
    }

    public function addNewOffset(GmtOffset $offset)
    {
    }

    public function updateOffset(GmtOffset $offset)
    {
    }
}