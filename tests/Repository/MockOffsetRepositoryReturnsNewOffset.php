<?php

declare(strict_types=1);

namespace Ibragimov\MiraiTestTask\Tests\Repository;

use Ibragimov\MiraiTestTask\Entity\GmtOffset;
use Ibragimov\MiraiTestTask\Repository\GmtOffsetRepository;

/**
 * Class MockOffsetRepositoryReturnsNull
 * @author Shapi Ibragimov <ibragimych26@gmail.com>
 */
final class MockOffsetRepositoryReturnsNewOffset extends GmtOffsetRepository
{
    private ?GmtOffset $offset = null;
    public function findOffsetForTimestamp(string $cityId, int $timestamp): ?GmtOffset
    {
        return $this->offset;
    }

    public function updateOffset(GmtOffset $offset)
    {
        $this->addNewOffset($offset);
    }

    public function addNewOffset(GmtOffset $offset)
    {
        $this->offset = $offset;
    }

}