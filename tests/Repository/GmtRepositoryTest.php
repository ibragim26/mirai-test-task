<?php

declare(strict_types=1);

namespace Ibragimov\MiraiTestTask\Tests\Repository;

use Doctrine\ORM\EntityManager;
use Ibragimov\MiraiTestTask\Entity\City;
use Ibragimov\MiraiTestTask\Entity\GmtOffset;
use Ibragimov\MiraiTestTask\Repository\GmtOffsetRepository;
use Ibragimov\MiraiTestTask\Tests\KernelTestCase;

/**
 * Class GmtRepositoryTest
 * @author Shapi Ibragimov <ibragimych26@gmail.com>
 */
final class GmtRepositoryTest extends KernelTestCase
{
    private static City $city;

    protected function setUp(): void
    {
        self::bootKernel();
        $this->createTestCity();
        $this->addOffset(MockOffsetRepository::HOUR, new \DateTime('-1 hour'));
        $this->addOffset(0);
    }

    public function testGetLastOffset()
    {
        $repository = self::$container->get(GmtOffsetRepository::class);
        $offset = $repository->findLastOffset(self::$city->getId());

        $this->assertEquals(0, $offset->getGmtOffset());

        $offset = $repository->findOffsetForTimestamp(
            self::$city->getId(), (new \DateTime('-30 minutes'))->getTimestamp())
        ;
        $this->assertEquals(MockOffsetRepository::HOUR, $offset->getGmtOffset());
    }

    private function addOffset(int $offset = MockOffsetRepository::HOUR, \DateTime $validFrom = null, int $dstStart = 0, int $dstEnd = 0)
    {
        $offsetEntity = new GmtOffset(self::$city);
        $offsetEntity->setValidFrom($validFrom ?? new \DateTime());
        $offsetEntity->setDstStart($dstStart);
        $offsetEntity->setDstEnd($dstEnd);
        $offsetEntity->setGmtOffset($offset);

        $em = self::$container->get(EntityManager::class);
        $em->persist($offsetEntity);
        $em->flush();
    }

    protected function tearDown(): void
    {
        $em = self::$container->get(EntityManager::class);
        $em->remove(self::$city);
        $em->flush();
        $em->clear();
    }

    private function createTestCity()
    {
        self::$city = MockCityRepository::getTestCity();
        $em = self::$container->get(EntityManager::class);
        $em->persist(self::$city);
        $em->flush();
    }
}