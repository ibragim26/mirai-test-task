<?php

declare(strict_types=1);

namespace Ibragimov\MiraiTestTask\Tests\Repository;

use Doctrine\ORM\EntityManager;
use Ibragimov\MiraiTestTask\Entity\City;
use Ibragimov\MiraiTestTask\Repository\CityRepository;
use Ibragimov\MiraiTestTask\Tests\KernelTestCase;
use Ibragimov\MiraiTestTask\TimeManager\Exception\CityNotFoundException;

/**
 * Class CityRepositoryTest
 * @author Shapi Ibragimov <ibragimych26@gmail.com>
 */
final class CityRepositoryTest extends KernelTestCase
{
    private static City $city;

    protected function setUp(): void
    {

    }

    public function testGetCity()
    {
        self::bootKernel();
        $this->createTestCity();

        $repository = self::$container->get(CityRepository::class);
        $city = $repository->getCityById(self::$city->getId());

        $this->assertEquals($city->getId(), self::$city->getId());

        $this->removeCity();
    }

    public function testGetCityError()
    {
        $this->expectException(CityNotFoundException::class);

        $repository = self::$container->get(CityRepository::class);
        $repository->getCityById('123123');
    }

    private function createTestCity()
    {
        self::$city = MockCityRepository::getTestCity();
        $em = self::$container->get(EntityManager::class);
        $em->persist(self::$city);
        $em->flush();
    }

    private function removeCity()
    {
        $em = self::$container->get(EntityManager::class);
        $em->remove(self::$city);
        $em->flush();
    }
}