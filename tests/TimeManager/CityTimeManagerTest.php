<?php

declare(strict_types=1);

namespace Ibragimov\MiraiTestTask\Tests\TimeManager;

use Doctrine\ORM\EntityManager;
use Ibragimov\MiraiTestTask\Entity\City;
use Ibragimov\MiraiTestTask\Tests\KernelTestCase;
use Ibragimov\MiraiTestTask\Tests\Repository\MockCityRepository;
use Ibragimov\MiraiTestTask\Tests\Repository\MockOffsetRepository;
use Ibragimov\MiraiTestTask\Tests\Repository\MockOffsetRepositoryReturnsNewOffset;
use Ibragimov\MiraiTestTask\Tests\TimeZoneDB\MockTimeZoneDBManagerFactory;
use Ibragimov\MiraiTestTask\TimeManager\CityTimeManager;
use Ibragimov\MiraiTestTask\TimeManager\Exception\TimestampOutOfRangeException;

/**
 * Class CityTimeManagerTest
 * @author Shapi Ibragimov <ibragimych26@gmail.com>
 */
final class CityTimeManagerTest extends KernelTestCase
{
    protected function setUp(): void
    {
        self::bootKernel();
    }

    public function testCreateGmtTimeFromLocal()
    {
        $localTime = (new \DateTime('now'))->getTimestamp();

        /** UTC+0 */
        $this->assertEquals(
            $localTime,
            $this->createTimeManager(0)->getGmtTimeFromLocal('', $localTime)
        );

        /** UTC+3 */
        $this->assertEquals(
            $localTime - MockOffsetRepository::HOUR * 3,
            $this
                ->createTimeManager(MockOffsetRepository::HOUR * 3)
                ->getGmtTimeFromLocal('', $localTime)
        );

        /** UTC-3 */
        $this->assertEquals(
            $localTime + MockOffsetRepository::HOUR * 3,
            $this
                ->createTimeManager(-MockOffsetRepository::HOUR * 3)
                ->getGmtTimeFromLocal('', $localTime)
        );
    }

    public function testCreateGmtTimeFromLocalWithDst()
    {
        $localTime = (new \DateTime('now'))->getTimestamp();
        $dstStart  = new \DateTime('-1 day');
        $dstEnd    = new \DateTime('+1 day');

        /** UTC+0 */
        $this->assertEquals(
            $localTime - MockOffsetRepository::HOUR,
            $this->createTimeManager(0, $dstStart, $dstEnd)->getGmtTimeFromLocal('', $localTime)
        );

        /** UTC+3 */
        $this->assertEquals(
            $localTime - MockOffsetRepository::HOUR * 4,
            $this
                ->createTimeManager(MockOffsetRepository::HOUR * 3, $dstStart, $dstEnd)
                ->getGmtTimeFromLocal('', $localTime)
        );

        /** UTC-3 */
        $this->assertEquals(
            $localTime + MockOffsetRepository::HOUR * 2,
            $this
                ->createTimeManager(-MockOffsetRepository::HOUR * 3, $dstStart, $dstEnd)
                ->getGmtTimeFromLocal('', $localTime)
        );
    }

    public function testCreateLocalTimeFromGmt()
    {
        $gmtTime = (new \DateTime('now'))->getTimestamp();

        /** UTC+0 */
        $this->assertEquals(
            $gmtTime,
            $this->createTimeManager(0)->getLocalTime('', $gmtTime)
        );

        /** UTC+3 */
        $this->assertEquals(
            $gmtTime + MockOffsetRepository::HOUR * 3,
            $this
                ->createTimeManager(MockOffsetRepository::HOUR * 3)
                ->getLocalTime('', $gmtTime)
        );

        /** UTC-3 */
        $this->assertEquals(
            $gmtTime - MockOffsetRepository::HOUR * 3,
            $this
                ->createTimeManager(-MockOffsetRepository::HOUR * 3)
                ->getLocalTime('', $gmtTime)
        );
    }

    public function testCreateLocalTimeFromGmtWithDst()
    {
        $gmtTime = (new \DateTime('now'))->getTimestamp();
        $dstStart  = new \DateTime('-1 day');
        $dstEnd    = new \DateTime('+1 day');

        /** UTC+0 */
        $this->assertEquals(
            $gmtTime + MockOffsetRepository::HOUR,
            $this->createTimeManager(0, $dstStart, $dstEnd)->getLocalTime('', $gmtTime)
        );

        /** UTC+3 */
        $this->assertEquals(
            $gmtTime + MockOffsetRepository::HOUR * 4,
            $this
                ->createTimeManager(MockOffsetRepository::HOUR * 3, $dstStart, $dstEnd)
                ->getLocalTime('', $gmtTime)
        );

        /** UTC-3 */
        $this->assertEquals(
            $gmtTime - MockOffsetRepository::HOUR * 2,
            $this
                ->createTimeManager(-MockOffsetRepository::HOUR * 3, $dstStart, $dstEnd)
                ->getLocalTime('', $gmtTime)
        );
    }

    public function testManagerCreatesNewOffset()
    {
        $em = self::$container->get(EntityManager::class);

        $manager = new CityTimeManager(
            new MockCityRepository($em),
            new MockOffsetRepositoryReturnsNewOffset($em),
            MockTimeZoneDBManagerFactory::createManager()
        );

        $gmtTime = (new \DateTime('now'))->getTimestamp();

        $this->assertEquals(
            $gmtTime + MockOffsetRepository::HOUR,
            $manager->getLocalTime('', $gmtTime)
        );
    }

    public function testManagerZeroTimestamp()
    {
        $this->expectException(TimestampOutOfRangeException::class);
        $city = self::$container->get(EntityManager::class)->getRepository(City::class)->findAll()[0];
        $manager = self::$container->get(CityTimeManager::class);

        $manager->getLocalTime($city->getId(), 0);
    }

    public function testManagerHugeTimestamp()
    {
        $this->expectException(TimestampOutOfRangeException::class);
        $city = self::$container->get(EntityManager::class)->getRepository(City::class)->findAll()[0];
        $manager = self::$container->get(CityTimeManager::class);

        $manager->getLocalTime($city->getId(), (new \DateTime('+5 moths'))->getTimestamp());
    }

    private function createTimeManager(int $offset = null, \DateTime $dstStart = null, \DateTime $dstEnd = null): CityTimeManager
    {
        $em = self::$container->get(EntityManager::class);
        return new CityTimeManager(
            new MockCityRepository($em),
            new MockOffsetRepository($em, $offset, $dstStart, $dstEnd),
            MockTimeZoneDBManagerFactory::createManager(
                $offset,
                $dstStart === null ? 0 : $dstStart->getTimestamp(),
                $dstEnd === null ? 0 : $dstEnd->getTimestamp()
            )
        );
    }
}