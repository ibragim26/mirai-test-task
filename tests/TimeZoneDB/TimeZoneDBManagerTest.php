<?php

declare(strict_types=1);

namespace Ibragimov\MiraiTestTask\Tests\TimeZoneDB;

use Ibragimov\MiraiTestTask\Tests\KernelTestCase;
use Ibragimov\MiraiTestTask\TimeZoneDB\TimeZoneDBManager;
use Symfony\Component\HttpClient\MockHttpClient;
use Symfony\Component\HttpClient\Response\MockResponse;

/**
 * Class TimeZoneDBManagerTest
 * @author Shapi Ibragimov <ibragimych26@gmail.com>
 */
final class TimeZoneDBManagerTest extends KernelTestCase
{
    public function testManager()
    {
        $manager = MockTimeZoneDBManagerFactory::createManager(-14400, 1615705200, 1636264800);

        $offsetModel = $manager->getGmtOffsetByCoordinates(40.7833, -91.1250);

        $this->assertEquals(
            -14400, $offsetModel->getGmtOffset()
        );
        $this->assertEquals(
            1615705200, $offsetModel->getDstStart()
        );
        $this->assertEquals(
            1636264800, $offsetModel->getDstEnd()
        );
    }
}