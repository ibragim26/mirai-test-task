<?php

declare(strict_types=1);

namespace Ibragimov\MiraiTestTask\Tests\TimeZoneDB;

use Ibragimov\MiraiTestTask\Tests\Repository\MockOffsetRepository;
use Ibragimov\MiraiTestTask\TimeZoneDB\TimeZoneDBManager;
use Symfony\Component\HttpClient\MockHttpClient;
use Symfony\Component\HttpClient\Response\MockResponse;

/**
 * Class MockTimeZoneDBManager
 * @author Shapi Ibragimov <ibragimych26@gmail.com>
 */
final class MockTimeZoneDBManagerFactory
{
    public static function createManager(int $gmtOffset = MockOffsetRepository::HOUR, int $dstStart = 0, int $dstEnd = 0)
    {
        return new TimeZoneDBManager(
            '123123',
            new MockHttpClient(
                new MockResponse(
                    json_encode([
                        'status' => 'OK',
                        'dst' => (int)($dstStart !== 0),
                        'zoneStart' => $dstStart,
                        'zoneEnd' => $dstEnd,
                        'gmtOffset' => $gmtOffset
                    ])
                )
            )
        );
    }
}