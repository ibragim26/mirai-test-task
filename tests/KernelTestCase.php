<?php

declare(strict_types=1);

namespace Ibragimov\MiraiTestTask\Tests;

use Ibragimov\MiraiTestTask\Kernel;
use PHPUnit\Framework\TestCase;
use Psr\Container\ContainerInterface;
use Symfony\Component\Dotenv\Dotenv;

/**
 * Class KernelTest
 * @author Shapi Ibragimov <ibragimych26@gmail.com>
 */
abstract class KernelTestCase extends TestCase
{
    protected static ?Kernel $kernel = null;
    protected static ?ContainerInterface $container = null;

    public function __construct(?string $name = null, array $data = [], $dataName = '')
    {
        $dotEnv = new Dotenv();
        $dotEnv->load(__DIR__ . '/../.env.test');
        parent::__construct($name, $data, $dataName);
    }

    public static function bootKernel()
    {
        if (self::$kernel === null) {
            self::$kernel = new Kernel();
            self::$container = self::$kernel->getContainer();
        }
    }
}