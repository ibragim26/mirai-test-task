<?php

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Tools\Console\ConsoleRunner;
use Ibragimov\MiraiTestTask\Kernel;
use Symfony\Component\Dotenv\Dotenv;

require_once __DIR__ . '/vendor/autoload.php';

$dotenv = new Dotenv();
$dotenv->load(__DIR__ . '/.env');

$entityManager = (new Kernel())->getContainer()->get(EntityManager::class);
return ConsoleRunner::createHelperSet($entityManager);
