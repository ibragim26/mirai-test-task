<?php

declare(strict_types=1);

namespace Ibragimov\MiraiTestTask\Configuration;

use Ibragimov\MiraiTestTask\Controller\TimeApiController;
use Ibragimov\MiraiTestTask\Exception\PrintableException;
use Pecee\Http\Request;
use Pecee\SimpleRouter\SimpleRouter;

/**
 * Class RouterConfiguration
 * @author Shapi Ibragimov <ibragimych26@gmail.com>
 */
final class RouterConfiguration
{
    public function register()
    {
        SimpleRouter::group([], function () {
            SimpleRouter::controller('/', TimeApiController::class);
        });
        SimpleRouter::error(function (Request $request, \Exception $error) {
            $response = SimpleRouter::response();
            if (!$error instanceof PrintableException) {
                $response->httpCode(500);
                $response->json(['message' => 'Внутренняя ошибка сервера.']);
            }

            $response->httpCode(400);
            $response->json(['message' => $error->getMessage()]);
        });
    }
}