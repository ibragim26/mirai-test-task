<?php

declare(strict_types=1);

namespace Ibragimov\MiraiTestTask\Configuration\Doctrine;

use Doctrine\Common\Cache\Cache;
use Doctrine\Common\Cache\PhpFileCache;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Tools\Setup;
use Ibragimov\MiraiTestTask\Configuration\ServiceFactoryInterface;
use Psr\Container\ContainerInterface;
use Symfony\Component\Filesystem\Filesystem;

/**
 * Class DoctrineFactory
 * @author Shapi Ibragimov <ibragimych26@gmail.com>
 */
final class DoctrineFactory implements ServiceFactoryInterface
{
    public static function create(ContainerInterface $container): EntityManager
    {
        $entities = $container->get('doctrine.entities');

        if (!is_array($entities)) {
            $entities = [$entities];
        }

        $devMode = $container->get('env') === 'dev' || $container->get('env') === 'test';

        $cacheDir = $container->get('cache_dir') . '/doctrine';
        $proxyDir = $container->get('cache_dir') . '/doctrine/proxy';

        $config = Setup::createAnnotationMetadataConfiguration(
            $entities,
            $devMode,
            $proxyDir,
            new PhpFileCache($cacheDir),
            false
        );

        $config->setAutoGenerateProxyClasses(!$devMode);

        return EntityManager::create(
            [
                'url' => $container->get('doctrine.database_url'),
                'defaultTableOptions' => [
                    'charset' => 'UTF8',
                    'collate' => 'utf8_general_ci'
                ]
            ],
            $config
        );
    }
}