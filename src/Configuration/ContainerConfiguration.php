<?php

declare(strict_types=1);

namespace Ibragimov\MiraiTestTask\Configuration;

use DI\Definition\Helper\FactoryDefinitionHelper;
use Doctrine\ORM\EntityManager;
use Ibragimov\MiraiTestTask\Configuration\Doctrine\DoctrineFactory;
use Ibragimov\MiraiTestTask\TimeZoneDB\TimeZoneDBManager;
use Psr\Container\ContainerInterface;

use Symfony\Component\HttpClient\HttpClient;

use function DI\create;
use function DI\factory;
use function DI\get;

/**
 * Class ContainerConfiguration
 * @author Shapi Ibragimov <ibragimych26@gmail.com>
 */
final class ContainerConfiguration
{
    public static function getDefinitions(): array
    {
        return [
            EntityManager::class => self::fromFactory(DoctrineFactory::class),

            TimeZoneDBManager::class => create(TimeZoneDBManager::class)
                ->constructor(get('timezonedb.api_key'), get('timezonedb.http_client')),

            'timezonedb.http_client' => fn(ContainerInterface $c) => HttpClient::create()
        ];
    }

    public static function getParameters(): array
    {
        return [
            'env' => $_ENV['APP_ENV'],
            'cache_dir' => $_ENV['CACHE_DIR'],
            'timezonedb.api_key' => $_ENV['TIMEZONEDB_API_KEY'],
            'doctrine.entities' => [
                __DIR__ . '/../Entity'
            ],
            'doctrine.database_url' => $_ENV['DATABASE_URL']
        ];
    }

    private static function fromFactory(string $serviceFactoryClass): FactoryDefinitionHelper
    {
        if (!in_array(ServiceFactoryInterface::class, class_implements($serviceFactoryClass))) {
            throw new \InvalidArgumentException('Service factory must implements ' . ServiceFactoryInterface::class);
        }


        return factory(function (ContainerInterface $c, string $serviceFactoryClass) {
            return $serviceFactoryClass::create($c);
        })->parameter('serviceFactoryClass', $serviceFactoryClass);
    }
}