<?php

namespace Ibragimov\MiraiTestTask\Configuration;

use Psr\Container\ContainerInterface;

/**
 * Interface ServiceFactoryInterface
 * @author Shapi Ibragimov <ibragimych26@gmail.com>
 */
interface ServiceFactoryInterface
{
    public static function create(ContainerInterface $container): object;
}