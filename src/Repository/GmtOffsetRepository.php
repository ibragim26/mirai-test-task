<?php

declare(strict_types=1);

namespace Ibragimov\MiraiTestTask\Repository;

use DateTime;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Ibragimov\MiraiTestTask\Entity\GmtOffset;

/**
 * Class OffsetRepository
 * @author Shapi Ibragimov <ibragimych26@gmail.com>
 */
class GmtOffsetRepository
{
    /**
     * @var EntityManager
     */
    private EntityManager $em;

    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    /**
     * @param string $cityId
     * @param int $timestamp
     * @return ?GmtOffset
     * @throws \Exception
     */
    public function findOffsetForTimestamp(string $cityId, int $timestamp): ?GmtOffset
    {
        $dateTime = (new DateTime('@'.$timestamp))
            ->setTimezone(new \DateTimeZone(date_default_timezone_get()));

        /** @var GmtOffset $offset */
        $offset = $this->em
            ->getRepository(GmtOffset::class)
            ->createQueryBuilder('go')
            ->where('go.validFrom <= :dateTime')
            ->andWhere('go.city = :city')
            ->orderBy('go.validFrom', 'desc')
            ->setMaxResults(1)
            ->setParameter('city', $cityId)
            ->setParameter('dateTime', $dateTime)
            ->getQuery()
            ->getOneOrNullResult()
            ;

        return $offset;
    }

    /**
     * @param string $cityId
     * @return GmtOffset
     * @throws \Exception
     */
    public function findLastOffset(string $cityId): ?GmtOffset
    {
        return $this->findOffsetForTimestamp($cityId, (new DateTime())->getTimestamp());
    }

    /**
     * @param GmtOffset $offset
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function resetUpdatedDateTime(GmtOffset $offset)
    {
        $offset->setUpdated(new DateTime());
        $this->em->flush();
    }

    /**
     * @param GmtOffset $offset
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function addNewOffset(GmtOffset $offset)
    {
        $this->em->persist($offset);
        $this->em->flush();
    }
}