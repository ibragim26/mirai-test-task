<?php

declare(strict_types=1);

namespace Ibragimov\MiraiTestTask\Repository;

use Doctrine\ORM\EntityManager;
use Ibragimov\MiraiTestTask\Entity\City;
use Ibragimov\MiraiTestTask\TimeManager\Exception\CityNotFoundException;

/**
 * Class CityRepository
 * @author Shapi Ibragimov <ibragimych26@gmail.com>
 */
class CityRepository
{
    /**
     * @var EntityManager
     */
    private EntityManager $em;

    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    /**
     * @param string $cityId
     * @return City
     * @throws CityNotFoundException
     */
    public function getCityById(string $cityId): City
    {
        /** @var City $city */
        $city = $this->em->find(City::class, $cityId);

        if ($city === null) {
            throw new CityNotFoundException('Город не найден.');
        }

        return $city;
    }
}