<?php

declare(strict_types=1);

namespace Ibragimov\MiraiTestTask\Controller;

use Doctrine\ORM\EntityManager;
use Ibragimov\MiraiTestTask\Entity\City;
use Ibragimov\MiraiTestTask\TimeManager\CityTimeManager;
use Ibragimov\MiraiTestTask\TimeManager\Exception\AbstractPrintableTimeManagerException;
use Ibragimov\MiraiTestTask\TimeManager\Exception\CityNotFoundException;
use Ibragimov\MiraiTestTask\TimeManager\Exception\TimestampOutOfRangeException;
use Pecee\Http\Request;
use Pecee\SimpleRouter\Handlers\IExceptionHandler;
use Pecee\SimpleRouter\SimpleRouter;

/**
 * Class TimeApiController
 * @author Shapi Ibragimov <ibragimych26@gmail.com>
 */
final class TimeApiController
{
    /**
     * @var CityTimeManager
     */
    private CityTimeManager $timeManager;
    /**
     * @var EntityManager
     */
    private EntityManager $em;

    public function __construct(CityTimeManager $timeManager, EntityManager $em)
    {
        $this->timeManager = $timeManager;
        $this->em = $em;
    }

    /**
     * @param string $cityId
     * @param int $timestamp
     * @throws CityNotFoundException
     * @throws TimestampOutOfRangeException
     */
    public function getLocalTime(string $cityId = '', int $timestamp = 0)
    {
        SimpleRouter::response()->json(
            ['localTime' => $this->timeManager->getLocalTime($cityId, $timestamp)]
        );
    }

    /**
     * @param string $cityId
     * @param int $timestamp
     * @throws CityNotFoundException
     * @throws TimestampOutOfRangeException
     */
    public function getGmtTime(string $cityId = '', int $timestamp = 0)
    {
        SimpleRouter::response()->json(
            ['gmtTime' => $this->timeManager->getGmtTimeFromLocal($cityId, $timestamp)]
        );
    }

    public function getCities()
    {
        SimpleRouter::response()->json(
            $this->em->getRepository(City::class)->findAll()
        );
    }
}