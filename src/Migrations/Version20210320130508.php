<?php

declare(strict_types=1);

namespace Ibragimov\MiraiTestTask\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210320130508 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE gmt_offsets (id CHAR(36) NOT NULL COMMENT \'(DC2Type:guid)\', city_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\', validFrom DATETIME NOT NULL, validTo DATETIME DEFAULT NULL, gmtOffset VARCHAR(255) NOT NULL, dstStart VARCHAR(255) NOT NULL, dstEnd VARCHAR(255) NOT NULL, INDEX IDX_17D0064F8BAC62AF (city_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_general_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE gmt_offsets ADD CONSTRAINT FK_17D0064F8BAC62AF FOREIGN KEY (city_id) REFERENCES cities (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE gmt_offsets');
        $this->addSql('CREATE UNIQUE INDEX cities_id_uindex ON cities (id)');
    }
}
