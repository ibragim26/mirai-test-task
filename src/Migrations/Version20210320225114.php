<?php

declare(strict_types=1);

namespace Ibragimov\MiraiTestTask\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210320225114 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE gmt_offsets ADD updated DATETIME NOT NULL DEFAULT "1970-01-01 00:00:00", DROP validTo');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE gmt_offsets ADD validTo DATETIME DEFAULT NULL, DROP updated');
    }
}
