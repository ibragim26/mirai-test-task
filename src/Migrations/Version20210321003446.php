<?php

declare(strict_types=1);

namespace Ibragimov\MiraiTestTask\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210321003446 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE gmt_offsets DROP FOREIGN KEY FK_17D0064F8BAC62AF');
        $this->addSql('ALTER TABLE gmt_offsets ADD CONSTRAINT FK_17D0064F8BAC62AF FOREIGN KEY (city_id) REFERENCES cities (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE gmt_offsets DROP FOREIGN KEY FK_17D0064F8BAC62AF');
        $this->addSql('ALTER TABLE gmt_offsets ADD CONSTRAINT FK_17D0064F8BAC62AF FOREIGN KEY (city_id) REFERENCES cities (id)');
    }
}
