<?php

declare(strict_types=1);

namespace Ibragimov\MiraiTestTask\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class City
 * @author Shapi Ibragimov <ibragimych26@gmail.com>
 *
 * @ORM\Entity
 * @ORM\Table(name="cities")
 *
 */
class City implements \JsonSerializable
{
    /**
     * @var string|null
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="UUID")
     * @ORM\Column(name="id", type="guid")
     */
    private ?string $id = null;
    /**
     * @var string
     * @ORM\Column
     */
    private string $countryIso3 = '';
    /**
     * @var string
     * @ORM\Column
     */
    private string $name = '';
    /**
     * @var float
     * @ORM\Column
     */
    private float $latitude = 0.0;
    /**
     * @var float
     * @ORM\Column
     */
    private float $longitude = 0.0;

    public function __construct(string $id = null)
    {
        if ($id !== null) {
            $this->id = $id;
        }
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id ?? '';
    }

    /**
     * @return string
     */
    public function getCountryIso3(): string
    {
        return $this->countryIso3;
    }

    /**
     * @param string $countryIso3
     * @return City
     */
    public function setCountryIso3(string $countryIso3): City
    {
        $this->countryIso3 = $countryIso3;
        return $this;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return City
     */
    public function setName(string $name): City
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return float
     */
    public function getLatitude(): float
    {
        return $this->latitude;
    }

    /**
     * @param float $latitude
     * @return City
     */
    public function setLatitude(float $latitude): City
    {
        $this->latitude = $latitude;
        return $this;
    }

    /**
     * @return float
     */
    public function getLongitude(): float
    {
        return $this->longitude;
    }

    /**
     * @param float $longitude
     * @return City
     */
    public function setLongitude(float $longitude): City
    {
        $this->longitude = $longitude;
        return $this;
    }

    public function jsonSerialize()
    {
        $props = [];

        foreach ($this as $propName => $propValue) {
            $props[$propName] = $propValue;
        }

        return $props;
    }
}