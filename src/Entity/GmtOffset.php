<?php

declare(strict_types=1);

namespace Ibragimov\MiraiTestTask\Entity;

use DateTime;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Entity;

/**
 * Class GmtOffset
 * @author Shapi Ibragimov <ibragimych26@gmail.com>
 *
 * @Entity
 * @ORM\Table(name="gmt_offsets")
 */
class GmtOffset
{
    /**
     * @var string|null
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="UUID")
     * @ORM\Column(name="id", type="guid")
     */
    private ?string $id = null;
    /**
     * @var City
     * @ORM\ManyToOne(targetEntity="Ibragimov\MiraiTestTask\Entity\City")
     * @ORM\JoinColumn(name="city_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private City $city;
    /**
     * @var DateTime
     * @ORM\Column(type="datetime")
     */
    private DateTime $validFrom;
    /**
     * @var DateTime
     * @ORM\Column(type="datetime")
     */
    private DateTime $updated;
    /**
     * @var int
     * @ORM\Column
     */
    private int $gmtOffset = 0;
    /**
     * @var int
     * @ORM\Column
     */
    private int $dstStart = 0;
    /**
     * @var int
     * @ORM\Column
     */
    private int $dstEnd = 0;

    public function __construct(City $city)
    {
        $this->setCity($city);
        $this->validFrom = new DateTime();
        $this->updated = new DateTime();
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id ?? '';
    }

    /**
     * @return City
     */
    public function getCity(): City
    {
        return $this->city;
    }

    /**
     * @param City $city
     * @return GmtOffset
     */
    public function setCity(City $city): GmtOffset
    {
        $this->city = $city;
        return $this;
    }

    /**
     * @return DateTime
     */
    public function getValidFrom(): DateTime
    {
        return $this->validFrom;
    }

    /**
     * @param DateTime $validFrom
     * @return GmtOffset
     */
    public function setValidFrom(DateTime $validFrom): GmtOffset
    {
        $this->validFrom = $validFrom;
        return $this;
    }

    /**
     * @return DateTime
     */
    public function getUpdated(): DateTime
    {
        return $this->updated;
    }

    /**
     * @param DateTime $updated
     * @return GmtOffset
     */
    public function setUpdated(DateTime $updated): GmtOffset
    {
        $this->updated = $updated;
        return $this;
    }

    /**
     * @return int
     */
    public function getGmtOffset(): int
    {
        return $this->gmtOffset;
    }

    /**
     * @param int $gmtOffset
     * @return GmtOffset
     */
    public function setGmtOffset(int $gmtOffset): GmtOffset
    {
        $this->gmtOffset = $gmtOffset;
        return $this;
    }

    /**
     * @return int
     */
    public function getDstStart(): int
    {
        return $this->dstStart;
    }

    /**
     * @param int $dstStart
     * @return GmtOffset
     */
    public function setDstStart(int $dstStart): GmtOffset
    {
        $this->dstStart = $dstStart;
        return $this;
    }

    /**
     * @return int
     */
    public function getDstEnd(): int
    {
        return $this->dstEnd;
    }

    /**
     * @param int $dstEnd
     * @return GmtOffset
     */
    public function setDstEnd(int $dstEnd): GmtOffset
    {
        $this->dstEnd = $dstEnd;
        return $this;
    }
}