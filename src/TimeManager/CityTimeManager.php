<?php

declare(strict_types=1);

namespace Ibragimov\MiraiTestTask\TimeManager;

use DateTime;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Exception;
use Ibragimov\MiraiTestTask\Entity\City;
use Ibragimov\MiraiTestTask\Entity\GmtOffset;
use Ibragimov\MiraiTestTask\Repository\CityRepository;
use Ibragimov\MiraiTestTask\Repository\GmtOffsetRepository;
use Ibragimov\MiraiTestTask\TimeManager\Exception\CannotUpdateOffsetException;
use Ibragimov\MiraiTestTask\TimeManager\Exception\TimestampOutOfRangeException;
use Ibragimov\MiraiTestTask\TimeZoneDB\Exception\TimeZoneDBServerException;
use Ibragimov\MiraiTestTask\TimeZoneDB\Model\TimeZoneDBGmtOffsetModel;
use Ibragimov\MiraiTestTask\TimeZoneDB\TimeZoneDBManager;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\DecodingExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;

/**
 * Class CityTimeManager
 * @author Shapi Ibragimov <ibragimych26@gmail.com>
 */
final class CityTimeManager implements CityTimeManagerInterface
{
    /**
     * Время жизни записи
     */
    private const OFFSET_LIFETIME = '-1 week';
    /**
     * кол-во секунд в часе
     */
    private const HOUR = 3600;

    private CityRepository $cityRepository;
    private GmtOffsetRepository $offsetRepository;
    private TimeZoneDBManager $timeZoneDBManager;

    public function __construct(
        CityRepository $cityRepository,
        GmtOffsetRepository $offsetRepository,
        TimeZoneDBManager $timeZoneDBManager
    ) {
        $this->cityRepository = $cityRepository;
        $this->offsetRepository = $offsetRepository;
        $this->timeZoneDBManager = $timeZoneDBManager;
    }

    /**
     * @param string $cityId
     * @param int $gmtTime
     * @return int
     * @throws Exception\CityNotFoundException
     * @throws Exception\TimestampOutOfRangeException
     */
    public function getLocalTime(string $cityId, int $gmtTime): int
    {
        $gmtOffset = $this->getGmtOffset($cityId, $gmtTime);
        return $gmtTime + $gmtOffset->getGmtOffset() + $this->getDstOffset($gmtTime, $gmtOffset);
    }

    /**
     * @param string $cityId
     * @param int $localTime
     * @return int
     * @throws Exception\CityNotFoundException
     * @throws Exception\TimestampOutOfRangeException
     */
    public function getGmtTimeFromLocal(string $cityId, int $localTime): int
    {
        $gmtOffset = $this->getGmtOffset($cityId, $localTime);
        return $localTime - $gmtOffset->getGmtOffset() - $this->getDstOffset($localTime, $gmtOffset);
    }

    /**
     * @param string $cityId
     * @param int $timestamp
     * @return GmtOffset
     * @throws TimestampOutOfRangeException
     */
    private function getGmtOffset(string $cityId, int $timestamp): GmtOffset
    {
        if ($timestamp > (new DateTime('+3 months'))->getTimestamp()) {
            throw new TimestampOutOfRangeException('Вычисление времени проводится не более чем на 3 месяца вперед');
        }

        $this->updateOffsetIfNeed($cityId);

        $offset = $this->offsetRepository->findOffsetForTimestamp($cityId, $timestamp);

        if ($offset === null) {
            throw new TimestampOutOfRangeException('Невозможно вычислить время.');
        }

        return $offset;
    }

    /**
     * @param string $cityId
     * @throws Exception
     */
    private function updateOffsetIfNeed(string $cityId)
    {
        $offset = $this->offsetRepository->findLastOffset($cityId);

        if ($offset === null || $this->isOutdated($offset->getUpdated())) {
            $this->updateOffset($cityId);
        }
    }

    /**
     * @param DateTime $updatedTime
     * @return bool
     */
    private function isOutdated(DateTime $updatedTime): bool
    {
        return $updatedTime < new DateTime(self::OFFSET_LIFETIME);
    }

    /**
     * @param int $timestamp
     * @param GmtOffset $gmtOffsetEntity
     * @return int
     */
    private function getDstOffset(int $timestamp, GmtOffset $gmtOffsetEntity): int
    {
        if ($gmtOffsetEntity->getDstStart() === null) {
            return 0;
        }

        if ($timestamp > $gmtOffsetEntity->getDstStart()
            && $timestamp < $gmtOffsetEntity->getDstEnd()) {
            return self::HOUR;
        }

        return 0;
    }

    /**
     * @param string $cityId
     * @return bool
     * @throws Exception\CityNotFoundException
     * @throws ORMException
     * @throws OptimisticLockException
     * @throws TimeZoneDBServerException
     * @throws ClientExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function updateOffset(string $cityId): bool
    {
        $city = $this->cityRepository->getCityById($cityId);
        $offsetEntity = $this->offsetRepository->findLastOffset($cityId);

        $offsetModel = $this->timeZoneDBManager->getGmtOffsetByCoordinates(
            $city->getLatitude(),
            $city->getLongitude()
        );

        if ($this->offsetsAreEqual($offsetModel, $offsetEntity)) {
            $this->offsetRepository->resetUpdatedDateTime($offsetEntity);
            return false;
        }

        $this->offsetRepository
            ->addNewOffset(
                $this->createNewOffset($city, $offsetModel)
            )
        ;

        return true;
    }

    /**
     * @param TimeZoneDBGmtOffsetModel $offsetModel
     * @param GmtOffset|null $offsetEntity
     * @return bool
     */
    private function offsetsAreEqual(TimeZoneDBGmtOffsetModel $offsetModel, ?GmtOffset $offsetEntity): bool
    {
        if ($offsetEntity === null) {
            return false;
        }

        if (
            $offsetModel->getGmtOffset() !== $offsetEntity->getGmtOffset() ||
            $offsetModel->getDstStart() !== $offsetEntity->getDstStart() ||
            $offsetModel->getDstEnd() !== $offsetEntity->getDstEnd()
        ) {
            return false;
        }

        return true;
    }

    /**
     * @param City $city
     * @param TimeZoneDBGmtOffsetModel $offsetModel
     * @return GmtOffset
     * @throws ORMException
     */
    private function createNewOffset(City $city, TimeZoneDBGmtOffsetModel $offsetModel): GmtOffset
    {
        $offsetEntity = new GmtOffset($city);
        $offsetEntity->setGmtOffset($offsetModel->getGmtOffset());

        if ($offsetModel->getDstStart() !== 0) {
            $offsetEntity
                ->setDstStart($offsetModel->getDstStart())
                ->setDstEnd($offsetModel->getDstEnd())
            ;
        }

        return $offsetEntity;
    }
}