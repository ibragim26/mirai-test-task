<?php

declare(strict_types=1);

namespace Ibragimov\MiraiTestTask\TimeManager;

use Ibragimov\MiraiTestTask\Entity\City;

/**
 * interface CityTimeManagerInterface
 * @author Shapi Ibragimov <ibragimych26@gmail.com>
 */
interface CityTimeManagerInterface
{
    public function getLocalTime(string $cityId, int $gmtTime): int;
    public function getGmtTimeFromLocal(string $cityId, int $localTime): int;
    public function updateOffset(string $cityId): bool;
}