<?php

declare(strict_types=1);

namespace Ibragimov\MiraiTestTask\TimeManager\Exception;

use Ibragimov\MiraiTestTask\Exception\PrintableException;

/**
 * Class CityNotFoundException
 * @author Shapi Ibragimov <ibragimych26@gmail.com>
 */
final class CityNotFoundException extends PrintableException
{

}