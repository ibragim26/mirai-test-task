<?php

declare(strict_types=1);

namespace Ibragimov\MiraiTestTask\TimeZoneDB\Model;

/**
 * Class TimeZoneDBGmtOffsetModel
 * @author Shapi Ibragimov <ibragimych26@gmail.com>
 */
final class TimeZoneDBGmtOffsetModel
{
    private int $gmtOffset;
    private int $dstStart;
    private int $dstEnd;

    /**
     * TimeZoneDBGmtOffsetModel constructor.
     * @param int $gmtOffset
     * @param int $dstStart
     * @param int $dstEnd
     */
    public function __construct(int $gmtOffset, int $dstStart, int $dstEnd)
    {
        $this->gmtOffset = $gmtOffset;
        $this->dstStart = $dstStart;
        $this->dstEnd = $dstEnd;
    }

    /**
     * @return int
     */
    public function getGmtOffset(): int
    {
        return $this->gmtOffset;
    }

    /**
     * @return int
     */
    public function getDstStart(): int
    {
        return $this->dstStart;
    }

    /**
     * @return int
     */
    public function getDstEnd(): int
    {
        return $this->dstEnd;
    }
}