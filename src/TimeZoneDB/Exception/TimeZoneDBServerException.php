<?php

declare(strict_types=1);

namespace Ibragimov\MiraiTestTask\TimeZoneDB\Exception;

/**
 * Class TimeZoneDBServerException
 * @author Shapi Ibragimov <ibragimych26@gmail.com>
 */
final class TimeZoneDBServerException extends \Exception
{

}