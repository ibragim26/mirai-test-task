<?php

declare(strict_types=1);

namespace Ibragimov\MiraiTestTask\TimeZoneDB;

use Ibragimov\MiraiTestTask\TimeZoneDB\Exception\TimeZoneDBServerException;
use Ibragimov\MiraiTestTask\TimeZoneDB\Model\TimeZoneDBGmtOffsetModel;
use Symfony\Component\HttpClient\HttpClient;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\DecodingExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

/**
 * Class TimeZoneDBManager
 * @author Shapi Ibragimov <ibragimych26@gmail.com>
 */
class TimeZoneDBManager
{
    private const API_URL = 'http://api.timezonedb.com/v2.1/get-time-zone';
    private string $apiKey;
    /**
     * @var HttpClientInterface
     */
    private HttpClientInterface $client;

    public function __construct(string $apiKey, HttpClientInterface $client)
    {
        $this->apiKey = $apiKey;
        $this->client = $client;
    }

    /**
     * @param float $lat
     * @param float $lng
     * @return TimeZoneDBGmtOffsetModel
     * @throws ClientExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TimeZoneDBServerException
     * @throws TransportExceptionInterface
     */
    public function getGmtOffsetByCoordinates(float $lat, float $lng): TimeZoneDBGmtOffsetModel
    {
        try {
            $response = $this->sendRequest($lat, $lng);
        } catch (\Exception $e) {
            throw new TimeZoneDBServerException($e->getMessage(), 0, $e);
        }

        if (!isset($response['status']) || $response['status'] !== 'OK') {
            throw new TimeZoneDBServerException($response['message'] ?? '');
        }

        if ((int)$response['dst'] === 1) {
            $dstStart = $response['zoneStart'];
            $dstEnd = $response['zoneEnd'];
        } else {
            $dstStart = $dstEnd = 0;
        }

        return new TimeZoneDBGmtOffsetModel(
            $response['gmtOffset'] ?? 0,
            $dstStart,
            $dstEnd
        );
    }

    /**
     * @param float $lat
     * @param float $lng
     * @return array
     * @throws ClientExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    private function sendRequest(float $lat, float $lng): array
    {
        return $this->client->request('GET', self::API_URL, [
            'query' => [
                'key' => $this->apiKey,
                'by' => 'position',
                'format' => 'json',
                'fields' => 'gmtOffset,zoneStart,zoneEnd,dst',
                'lat' => $lat,
                'lng' => $lng
            ]
        ])->toArray();
    }
}