<?php

declare(strict_types=1);

namespace Ibragimov\MiraiTestTask\Exception;

use Exception;

/**
 * Class PrintableException
 * @author Shapi Ibragimov <ibragimych26@gmail.com>
 */
abstract class PrintableException extends Exception
{

}