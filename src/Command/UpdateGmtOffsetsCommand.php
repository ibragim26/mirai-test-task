<?php

declare(strict_types=1);

namespace Ibragimov\MiraiTestTask\Command;

use Doctrine\ORM\EntityManager;
use Ibragimov\MiraiTestTask\Entity\City;
use Ibragimov\MiraiTestTask\TimeManager\CityTimeManager;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class UpdateGmtOffsetsCommand
 * @author Shapi Ibragimov <ibragimych26@gmail.com>
 */
final class UpdateGmtOffsetsCommand extends Command
{
    /**
     * @var EntityManager
     */
    private EntityManager $em;
    /**
     * @var CityTimeManager
     */
    private CityTimeManager $timeManager;

    public function __construct(EntityManager $em, CityTimeManager $timeManager)
    {
        parent::__construct('time:update_offsets');
        $this->em = $em;
        $this->timeManager = $timeManager;
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $cities = $this->getCities();

        $progress = new ProgressBar($output, count($cities));
        $progress->start();

        $counter = 0;
        foreach ($cities as $city) {
            $counter += (int)$this->timeManager->updateOffset($city->getId());
            $progress->advance();
            sleep(1); // hack for avoiding 503 error
        }

        $output->writeln('');
        $output->writeln('Данные о ' . $counter . ' городах обновлены.');
        $output->writeln('');

        $progress->finish();
        return 0;
    }

    /**
     * @return City[]
     */
    private function getCities(): array
    {
        return $this->em->getRepository(City::class)->findAll();
    }
}