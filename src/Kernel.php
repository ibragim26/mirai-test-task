<?php

declare(strict_types=1);

namespace Ibragimov\MiraiTestTask;

use DI\Container;
use DI\ContainerBuilder;
use Exception;
use Ibragimov\MiraiTestTask\Configuration\ContainerConfiguration;
use Ibragimov\MiraiTestTask\Configuration\RouterConfiguration;
use Pecee\Http\Middleware\Exceptions\TokenMismatchException;
use Pecee\SimpleRouter\Exceptions\HttpException;
use Pecee\SimpleRouter\Exceptions\NotFoundHttpException;
use Pecee\SimpleRouter\SimpleRouter;
use Psr\Container\ContainerInterface;
use Symfony\Component\Filesystem\Filesystem;

/**
 * Class Kernel
 * @author Shapi Ibragimov <ibragimych26@gmail.com>
 */
final class Kernel
{
    /**
     * @var Container
     */
    private Container $container;
    /**
     * @var string
     */
    private string $cacheDir;

    public function __construct()
    {
        if (!isset($_ENV['CACHE_DIR'])) {
            $_ENV['CACHE_DIR'] = sys_get_temp_dir() . '/mirai_test_task';
        }

        if ($_ENV['CACHE_DIR'][0] !== '/') {
            $_ENV['CACHE_DIR'] = __DIR__ . '/../' . $_ENV['CACHE_DIR'];
        }

        $this->createCacheDir($_ENV['CACHE_DIR']);

        $this->cacheDir =  $_ENV['CACHE_DIR'];
        $this->container = $this->createContainer();
    }

    /**
     * @throws TokenMismatchException
     * @throws HttpException
     * @throws NotFoundHttpException
     */
    public function start(): void
    {
        SimpleRouter::enableDependencyInjection($this->container);
        (new RouterConfiguration())->register();
        SimpleRouter::start();
    }

    /**
     * @return Container
     * @throws Exception
     */
    private function createContainer(): Container
    {
        $containerBuilder = new ContainerBuilder();

        if ($_ENV['APP_ENV'] === 'prod') {
            $containerBuilder
                ->enableCompilation($this->cacheDir)
                ->writeProxiesToFile(true, $this->cacheDir . '/proxies')
            ;
        }

        return $containerBuilder
            ->addDefinitions(
                array_merge(ContainerConfiguration::getDefinitions(), ContainerConfiguration::getParameters())
            )
            ->useAutowiring(true)
            ->build()
        ;
    }

    /**
     * @return ContainerInterface
     */
    public function getContainer(): ContainerInterface
    {
        return $this->container;
    }

    private function createCacheDir(string $cacheDir): void
    {
        $filesystem = new Filesystem();

        if ($filesystem->exists($cacheDir)) {
            return;
        }

        $filesystem->mkdir($cacheDir);
    }
}